const express = require('express');
const router = express.Router();
const db = require('mysql');
const conn = db.createPool({
    //removed from repository
});

let dbcon = {};

dbcon.event = (event) => {
    return new Promise((resolve, reject) => {
        conn.query("SELECT * FROM event WHERE id=?", [event], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.eventsAll = () => {
    return new Promise((resolve, reject) => {
        conn.query("SELECT * FROM event ORDER BY startTime ASC", (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}

dbcon.eventFilter = (user) => {
    return new Promise((resolve, reject) => {
        conn.query("SELECT * FROM event WHERE creator=?", [user], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    })
}


dbcon.allCity = () => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM city ORDER BY nameCity', (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.city = (name) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM city WHERE nameCity=?', [name], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.allType = () => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM type ORDER BY numEvents DESC', (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.type = (name) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM type WHERE nameType=?', [name], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.userRes = (user) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM reservations WHERE user=?', [user], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.eventRes = (id) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM reservations', (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.idRes = (id) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM reservations WHERE id=?', [id], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.getUser = (username, password) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM user WHERE username=? AND password=?', [username, password], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.get1User = (id) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM user WHERE id=?', [id], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.getEmail = (email, password) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM user WHERE email=? AND password=?', [email, password], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.allUsers = () => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM user', (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}

dbcon.userbycred = (cred) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM user WHERE username=? OR email=?', [cred,cred], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
};

dbcon.userbyMail = (email) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM user WHERE email=?', [email], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}

dbcon.userbyName = (user) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM user WHERE username=?', [user], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}

dbcon.countevs = (user) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT count(*) AS count FROM event WHERE creator=?', [user], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}

dbcon.countrevs = (user) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT count(*) AS count FROM reservations WHERE user=?', [user], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}

dbcon.finduser = (id) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM user WHERE id=?', [id], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}

dbcon.getreps = () => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM reports', (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}

dbcon.getusers = () => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM user', (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}


module.exports = dbcon;