const express = require('express');
const app = express();
const router = require('./routes');
const cors = require('cors');

var bodyParser = require('body-parser');

app.use(cors());
app.use(express.json());
app.use(bodyParser.json({limit: '900mb', parameterLimit:3000000})); 
app.use(bodyParser.urlencoded({limit: '900mb', extended: true, parameterLimit:3000000}));
app.use('/dapi', router);
PORT = process.env.PORT || '5000';

app.listen(PORT, () => {
    console.log(`Server started on PORT ${PORT}`);
});
