const express = require('express');
const db = require('../db');
const router = express.Router();
const sql = require('mysql');
const e = require('express');
const { response, query } = require('express');
const conn = sql.createPool({
    //removed from repository
});

router.get('/upload/:blob', async (req, res, next) => {
    res.json(req.params.blob);
})

router.get('/allcity', async (req, res, next) => {
    let results = await db.allCity();
    res.json(results);
})

router.get('/countrevs/:usr', async (req, res, next) => {
    let results = await db.countrevs(req.params.usr);
    res.json(results);
})

router.get('/countevs/:usr', async (req, res, next) => {
    let results = await db.countevs(req.params.usr);
    res.json(results);
})

router.get('/alltype', async (req, res, next) => {
    let results = await db.allType();
    res.json(results);
})

router.get('/report/:rep/:ev/:us', async (req, res, next) => {
    conn.query("INSERT INTO reports(report,userrep,eventrep) VALUES(?,?,?)", [req.params.rep, req.params.us, req.params.ev], (err, result) => {
        if (err) {
            res.json({ message: err });
        } else {
            res.json({ message: "success" });
        }
    })
})


router.post('/addevent', async (req, res, next) => {
    res.json(req.body);
    let name = req.body.name, desc = req.body.desc, url = req.body.blob.changingThisBreaksApplicationSecurity, city = req.body.city, type = req.body.type,
        start = req.body.date, price = req.body.price, tables = req.body.tables, creator = req.body.creator;
    conn.query("INSERT INTO event(eventName,eventDesc,url,city,type,startTime,status,price,numTables,resTables,creator) VALUES(?,?,?,?,?,?,?,?,?,?,?)",
        [name, desc, url, city, type, start, 1, price, tables, 0, creator], function(err, result) {
        if (err) throw err;
        console.log('record inserted');
  });
    
})

router.get('/reserve/:tables/:user/:event', async (req, res, next) => {
    let event = await db.event(req.params.event);
    let totalprice = 0; let price = event[0].price;
    for (let i = 0; i < parseInt(req.params.tables); i++) {
        totalprice += price;
    }
    num = Math.random();
    cid = num.toString();
    conn.query("UPDATE event SET resTables=? WHERE id=?", [event[0].resTables + parseInt(req.params.tables), event[0].id], (err, result) => { });
    conn.query("INSERT INTO reservations(confirmationId,tables,price,user,event) VALUES (?,?,?,?,?)", [cid, parseInt(req.params.tables),
        totalprice, req.params.user, parseInt(req.params.event)], (err, result) => {
            if (err) {
                message = err;
            } else {
                message = "OK";
        }
        let obj = {
        message: message
        }
        res.json(obj);
    });
})

router.get('/getevents/:user', async (req, res, next) => {
    let events = await db.eventFilter(req.params.user);
    res.json(events);
})

router.get('/geteventsall', async (req, res, next) => {
    let events = await db.eventsAll();
    res.json(events);
})

router.get('/getresev', async (req, res, next) => {
    let resev = await db.eventRes(req.params.id);
    res.json(resev);
})

router.get('/getresus/:user', async (req, res, next) => {
    let resus = await db.userRes(req.params.user);
    res.json(resus);
})

router.get('/cancelres/:id', async (req, res, next) => {
    let resus = await db.idRes(req.params.id);
    let event = await db.event(resus[0].event);
    conn.query("UPDATE event SET resTables=? WHERE id=?", [event[0].resTables - resus[0].tables, resus[0].event], (err, result) => { });
    conn.query("DELETE FROM reservations WHERE id=?", [parseInt(req.params.id)], (err, result) => {
        if (err) {
            obj = {
                message: err
            };
        } else {
            obj = {
                message: "Success"
            };
        }
        res.json(obj);
    })
})

router.get('/delevent/:id', async (req, res, next) => {
    let event = await db.event(req.params.id); let city = event[0].city; let type = event[0].type;
    conn.query("DELETE FROM reservations WHERE event=?", [event[0].id], (err, results) => { });
    conn.query("DELETE FROM event WHERE id=?", [parseInt(req.params.id)], (err, result) => {
        if (err) {
            obj = {
                message: err
            };
        } else {
            obj = {
                message: "Success"
            };
        }
        res.json(obj);
    })
})

router.post('/createAcc', async (req, res, next) => {
    let usermail = await db.userbyMail(req.body.eml);
    let username = await db.userbyName(req.body.usr);
    if (usermail.length > 0) {
        res.json({ message: "email already exists" });
    } else if (username.length > 0) {
        res.json({ message: "username already exists" });
    } else {
        conn.query("INSERT INTO user(username,password,email,type) VALUES (?,?,?,?)", [req.body.usr, req.body.psw, req.body.eml, "usr"], (err, result) => {
        if (err) {
            res.json({ message: err });
        } else {
            res.json({ message: "Account created" });
        }
    })
    }
})

router.get('/allUsers', async (req, res, next) => {
    let users = await db.allUsers();
    res.json(users);
})

router.get('/login/:cred/:psw', async (req, res, next) => {
    let loginu = await db.getUser(req.params.cred, req.params.psw);
    if (loginu.length == 0) {
        let logine = await db.getEmail(req.params.cred, req.params.psw);
        if (logine.length == 0) {
            res.json({ message: "Wrong combination" });
        } else {
            res.json({ message: "Logged In" });
        }
    } else {
        res.json({ message: "Logged In" });
    }
})

router.get('/terminate/:id', async (req, res, next) => {
    let user = await db.finduser(req.params.id);
    let message = "";
    conn.query("DELETE FROM event WHERE creator=?", [user[0].username], (err, result) => {
        if (err) {
            message += err;
        } else {
            message += "success events ";
        }
    })
    conn.query("DELETE FROM reservations WHERE user=?", [user[0].username], (err, result) => {
        if (err) {
            message += err;
        } else {
            message += "success res ";
        }
    })
    conn.query("DELETE FROM user WHERE id=?", [req.params.id], (err, result) => {
        if (err) {
            message += err;
        } else {
            message += "success del ";
        }
    })
    res.json({ message: message });
})

router.get('/updateAcc/:id/:usr/:email/:psw', async (req, res, next) => {
    let user = await db.get1User(req.params.id);
    conn.query("UPDATE event SET creator=? WHERE creator=?", [req.params.usr, user[0].username], (err, result) => { });
    conn.query("UPDATE reservations SET user=? WHERE user=?", [req.params.usr, user[0].username], (err, result) => { });
    conn.query("UPDATE user SET username=?,email=?,password=? WHERE id=?", [req.params.usr, req.params.email, req.params.psw, req.params.id], (err, result) => { });
    res.json(user[0].username);
})

router.get('/currentUser/:cred', async (req, res, next) => {
    let usr = await db.userbycred(req.params.cred);
    res.json(usr);
})

router.post('/updatevent', async (req, res, next) => {
    let name = req.body.name, desc = req.body.desc, city = req.body.city, type = req.body.type, price = req.body.price,
        tables = req.body.tickets, id=req.body.id, start = req.body.start;
    conn.query("UPDATE event SET eventName=?, eventDesc=?, city=?, type=?, startTime=?, price=?, numTables=? WHERE id=?",
        [name, desc, city, type, start, price, tables, id ], (err, result) => { 
            if (err) {
                inputstatus = err;
            } else {
                inputstatus = "OK";
            }
        let obj = {
            input: inputstatus
        }
        res.json(obj);
    });
})

router.get("/getevent/:id", async (req, res, next) => {
    let event = await db.event(req.params.id);
    res.json(event);
})

router.get("/getreps", async (req, res, next) => {
    let reps = await db.getreps();
    res.json(reps);
})

router.get('/promote/:id', async (req, res, next) => {
    conn.query("UPDATE user SET type=? WHERE id=?", ["adm", req.params.id], (err, result) => { });
    res.json({ message: "Account updated" });
})

router.get('/demote/:id', async (req, res, next) => {
    conn.query("UPDATE user SET type=? WHERE id=?", ["usr", req.params.id], (err, result) => { });
    res.json({ message: "Account updated" });
})

router.get("/getusers", async (req, res, next) => {
    let reps = await db.getusers();
    res.json(reps);
})

router.get("/dismissrep/:id", async (req, res, next) => {
    let msg;
    conn.query("DELETE FROM reports WHERE id=?", [req.params.id], (err, result) => {
        if (err) {
            msg = err;
        } else {
            msg = "ok";
        }
    });
    res.json({ message: msg });
})


module.exports = router;